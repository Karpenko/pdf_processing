PDF_PROCESSING Application
==========================

This is a simple PDF_PROCESSING application, which
allows uploading a .pdf file, and then access each
of its pages in as a separate .png image. 

Setup
-----

* Clone the application

* If mysql and/or rabbitmq are installed locally,
  you might need to stop these services, if ports
  of apps defined in docker-compose conflict with
  them.

* Export password as environment variable `PASS`

* Install docker-compose locally


Run
---

After after setting the application up, do the following:  

.. code-block:: shell
   
    $ docker-compose -f docker-compose.yaml build
    $ docker-compose up 

If all went fine, you should be able to navigate to: http://127.0.0.1:8000 in your browser and see the application.

To upload the file, send PUT request like:

.. code-block:: python

    >>> url = 'http://127.0.0.1:8000/documents/'
    >>> files = {'file': open(<your-file>, 'rb')}
    >>> requests.put(url=url, files=files)


If all went fine, you should receive id of your document in the response data. Then you could query http://127.0.0.1:8000/documents/id to see
the status of the documents. If status is `processing`, the number of pages would automatically be 0. As soon as status is `done`, you should see the number of pages updated.

Next, you could query http://127.0.0.1:8000/documents/id/pages/<page number> to access any page as .PNG image.


Testing
-------

To run the tests find the id of the `pdf-processing` container, enter it with:

.. code-block:: shell
   
    $ docker exec -it <container_id> /bin/sh
    # And, from withing the container
    $ ./manage.py test


