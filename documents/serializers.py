from rest_framework import serializers

from .models import PDF, Page


class PDFSerializer(serializers.ModelSerializer):

    pdf_file = serializers.FileField(write_only=True)
    n_pages = serializers.IntegerField(default=0)
    pages = []

    class Meta:
        model = PDF
        fields = ("id", "status", "pdf_file", "n_pages")
        read_only_fields = ("n_pages",)


class PageSerializer(serializers.ModelSerializer):

    pdf = serializers.PrimaryKeyRelatedField(queryset=PDF.objects.all())
    page = serializers.ImageField()
    page_n = serializers.IntegerField()

    class Meta:
        model = Page
        fields = ("id", "pdf", "page", "page_n")
