import dramatiq
from pdf2image import convert_from_bytes

from .models import PDF
from .helpers import save_image


@dramatiq.actor
def save_images(obj_id):

    pdf = PDF.objects.get(pk=obj_id)
    pdf_file = pdf.pdf_file
    try:
        pages = convert_from_bytes(pdf_file.read(), 500, size=(1200, 1600))
        pdf.n_pages = len(pages)
        for (i, page) in enumerate(pages):
            save_image(i, page, obj_id)
        pdf.status = PDF.STATUS_DONE
    except Exception:
        pdf.status = PDF.PDF_FAILED
    pdf.save()
