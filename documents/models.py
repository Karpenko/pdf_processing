from django.db import models


# Create your models here.
class PDF(models.Model):

    STATUS_PROCESSING = "processing"
    STATUS_FAILED = "failed"
    STATUS_DONE = "done"
    STATUSES = [

        (STATUS_PROCESSING, "Processing"),
        (STATUS_FAILED, "Failed"),
        (STATUS_DONE, "Done"),

    ]

    pdf_file = models.FileField(upload_to='./uploads/pdfs')
    status = models.CharField(max_length=20)
    n_pages = models.IntegerField(blank=False)


class Page(models.Model):

    pdf = models.ForeignKey(PDF,
                            related_name="belongs_to",
                            on_delete=models.DO_NOTHING)
    page = models.ImageField(upload_to='./uploads/pngs')
    page_n = models.IntegerField(blank=False)

