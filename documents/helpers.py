from django.core.files.base import ContentFile
from io import BytesIO


from .serializers import PageSerializer


def save_image(i, image, obj):

    img_io = BytesIO()
    image.save(img_io, format='png', quality=500)
    img_content = ContentFile(img_io.getvalue(), 'page.png')

    page_data = {'pdf': obj,
                 'page': img_content,
                 'page_n': i + 1}
    page_serializer = PageSerializer(data=page_data)
    if page_serializer.is_valid():
        page_serializer.save()
    else:
        print(page_serializer.errors)

