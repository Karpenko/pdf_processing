from django.urls import reverse
from django_dramatiq.test import DramatiqTestCase

from rest_framework.test import APITestCase, APIClient

from .models import PDF
from .tasks import save_images


class PDFTestCase(APITestCase):

    client = APIClient()

    def test_file_upload(self):

        url = reverse('documents:create_pdf')
        bach_file = open('bach.pdf', 'rb')
        response = self.client.put(url,
                                   {'file': bach_file},
                                   format='multipart')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {'id': 1})

    def test_file_info(self):

        self.test_file_upload()
        url = reverse('documents:view_pdf', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['status'], 'processing')


class PageProcessingTestCase(DramatiqTestCase):

    def test_number_of_pages(self):

        pdf = PDFTestCase()
        pdf.test_file_upload()
        save_images.send(1)
        self.broker.join(save_images.queue_name)
        self.worker.join()

        obj = PDF.objects.get(pk=1)
        assert obj.n_pages == 5
