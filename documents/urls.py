from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from . import views


app_name = "documents"
urlpatterns = [

    url(r"^$",
        csrf_exempt(views.MyUploadView.as_view()),
        name="create_pdf"),
    url(r"^(?P<pk>\d+)$",
        csrf_exempt(views.RetrievePdfView.as_view()),
        name="view_pdf"),
    url(r"^(?P<pdf_pk>\d+)/pages/(?P<pk>\d+)$",
        csrf_exempt(views.RetrievePageView.as_view()),
        name="view_page"),

]
