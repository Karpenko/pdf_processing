from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from django.http import FileResponse

from rest_framework.exceptions import ParseError
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import RetrieveAPIView

from .models import PDF, Page
from .serializers import PDFSerializer, PageSerializer
from .tasks import save_images


class MyUploadView(APIView):
    parser_class = (FileUploadParser,)

    @csrf_exempt
    def put(self, request, format=None):
        if 'file' not in request.FILES:
            raise ParseError("Empty content")

        file_obj = request.FILES['file']
        pdf_data = {'pdf_file': file_obj,
                    'status': PDF.STATUS_PROCESSING}
        serializer = PDFSerializer(data=pdf_data)
        if serializer.is_valid():
            obj = serializer.save()
            obj_id = obj.id
            save_images.send(obj_id)
            return Response({'id': obj_id})

        return Response({'errors': serializer.errors})


class RetrievePdfView(RetrieveAPIView):

    def get(self, request, pk=None):

        queryset = PDF.objects.all()
        pdf = get_object_or_404(queryset, pk=pk)
        serializer = PDFSerializer(pdf)
        return Response(serializer.data)


class RetrievePageView(RetrieveAPIView):

    def get(self, request, pdf_pk=None, pk=None):

        queryset = Page.objects.all()
        page = get_object_or_404(queryset, pdf=pdf_pk, page_n=pk)
        serializer = PageSerializer(page)
        page_address = './' + serializer.data['page']
        image_file = open(page_address, "rb")
        return FileResponse(image_file)


