FROM python:3.8.4

ENV PYTHONBUFFERED 1

COPY requirements.txt /pdf_processing/requirements.txt

WORKDIR /pdf_processing

RUN pip install -r requirements.txt
RUN apt-get update
RUN apt-get install poppler-utils -y

COPY . /pdf_processing/

